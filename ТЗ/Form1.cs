﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Net.Mime.MediaTypeNames;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.Button;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.Window;

namespace ТЗ
{
    public partial class API : Form
    {
        public API()
        {
            InitializeComponent();
        }
        // Массив с наименованиями объектов
        public string[] type = new string[]
        {
            "Random",
            "Line",
            "Rectangle",
            "Ellipse",
            "Text"
        }; 

        static Random rnd = new Random();
        int randomNum = rnd.Next(1, 4);
        
        void Form1_Load(object sender, EventArgs e)
        {
            
        }


        void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        void listBox1(object sender, EventArgs e)
        {


            //Координаты первой точки
            int x = 10;
            int y = 10;

            //Координтаы второй точки
            int width = 110;
            int height = 110;


            //Толщина и цвет линии
            int sizePen = 5;
            Color color = Color.Black;

            //Кисть для рисования
            Pen brush = new Pen(color, sizePen);
            Brush pen = new SolidBrush(color);

            Font font = new Font(FontFamily.GenericSansSerif, 15);
            
            // Записываю активное значение из ComboBox
            string activeVal = comboBox1.Text;


            Label lbl = new Label();
            lbl.Size = new Size(300, 20);
            lbl.Text = "";
            lbl.ForeColor = Color.White;
            this.Controls.Add(lbl);
            lbl.AutoSize = false;
            lbl.TextAlign = ContentAlignment.MiddleCenter;
            lbl.BringToFront();
 
            for (int i = 0; i < type.Length; i++)
            {
                // Проверяю на выбранного объекта из ComboBox и наличиев массиве, после чего делаю вывод объекта.
                // Пока на тексте проверяю логику. но что то идет не так.
                switch (activeVal)
                {
                    case "Random":
                        switch (randomNum)
                        {
                            
                            case 1:
                                lbl.Text = $"{type[i]}";
                                CreateLableDrawObj(x, y, height);
                                //DrawLine(g, brush, x, y, width, height);
                                break;
                            case 2:
                                lbl.Text = $"{type[i]}";
                                CreateLableDrawObj(x, y, height);
                                //DrawRectangle(g, brush, x, y, width, height);
                                break;
                            case 3:
                                lbl.Text = $"{type[i]}";
                                CreateLableDrawObj(x, y, height);
                                //DrawEllipse(g, brush, x, y, width, height);
                                break;
                            case 4:
                                lbl.Text = $"{type[i]}";
                                CreateLableDrawObj(x, y, height);
                                //DrawText(g, text, font, pen, x, y);
                                break;
                        }
                        break;
                    case $"{type[i]}":
                        lbl.Text = $"{activeVal}";
                        CreateLableDrawObj(x, y, height);
                        //DrawLine(g, brush, x, y, width, height);
                        break;
                    case $"{type[i]}:
                        lbl.Text = $"{activeVal}";
                        CreateLableDrawObj(x, y, height);
                        //DrawRectangle(g, brush, x, y, width, height);
                        break;
                    case $"{type[i]}:
                        lbl.Text = $"{activeVal}";
                        CreateLableDrawObj(x, y, height);
                        //DrawEllipse(g, brush, x, y, width, height);//Рисование эллипса
                        break;
                    case $"{type[i]}:
                        lbl.Text = $"{activeVal}";
                            CreateLableDrawObj(x, y, height); ;
                        //DrawText(g, text, font, pen, x, y);
                        break;
                } 
            }
        }    


        void Panel_Paint(object sender, PaintEventArgs e)
        {
            //Generation_Click(Generation,null);
            DrawObject(e);

            
        }
        // Не могу понять как вывести на листбокс выбранный объект
        void Generation_Click(object sender, EventArgs e)
        {
            listBox1(sender, e);
        }


        // Выборка с Комбобокса и проверка совпадения

        void DrawObject(PaintEventArgs e) 
        {
            //Координаты первой точки
            int x = 10;
            int y = 10;

            //Координтаы второй точки
            int width = 110;
            int height = 110;

            
            //Толщина и цвет линии
            int sizePen = 5;
            Color color = Color.Black;

            //Кисть для рисования
            Pen brush = new Pen(color, sizePen);
            Brush pen = new SolidBrush(color);

            Font font = new Font(FontFamily.GenericSansSerif, 15);
            string text = "Rendom Text";

          
            for (int i = 0; i < type.Length; i++)
            {
                switch (type[i])
                {
                    case "Random":
                        switch (randomNum)
                        {
                            case 1:
                                DrawLine(e, brush, x, y, width, height);
                                break;
                            case 2:
                                DrawRectangle(e, brush, x, y, width, height);
                                break;
                            case 3:
                                DrawEllipse(e, brush, x, y, width, height);//Рисование эллипса
                                break;
                            case 4:
                                DrawText(e, text, font, pen, x, y);
                                break;
                        }
                        break;
                    case "Line":
                        DrawLine(e, brush, x, y, width, height);
                        break;
                    case "Rectangle":
                        DrawRectangle(e, brush, x, y, width, height);
                        break;
                    case "Ellipse":
                        DrawEllipse(e, brush, x, y, width, height);//Рисование эллипса
                        break;
                    case "Text":
                        
                        DrawText(e, text, font, pen, x, y);
                        break;
                }
                CreateLableDrawObj(x, y, height);
                y += height + 30;

            }
        }

        public void DrawLine(PaintEventArgs e, Pen brush, int x, int y, int width, int height)
        {
            // Draw line to screen.
            height += y;
            e.Graphics.DrawLine(brush, x, y, width, height);

        }
        public void DrawRectangle(PaintEventArgs e, Pen brush, int x, int y, int width, int height)
        {
            // Рисование прямоугольника
            e.Graphics.DrawRectangle(brush, x, y, width, height);
        }
        public void DrawEllipse(PaintEventArgs e, Pen brush, int x, int y, int width, int height)
        {
            // Рисование Эллипса 
            e.Graphics.DrawEllipse(brush, x, y, width, height);
        }
        public void DrawText(PaintEventArgs e, string text, Font font, Brush brush, int x, int y)
        {
            // Рисование текста
            e.Graphics.DrawString(text, font, brush, x, y);
        }

        public void CreateLableDrawObj(int x, int y, int height) 
        {
            Label lbl = new Label();
            lbl.Size = new Size(35, 20);
            lbl.Text = "00" + (x + y) / 2;
            lbl.ForeColor = Color.White;
            this.Controls.Add(lbl);
            int centerX = (x - x + height) / 2;
            int centerY = (y + y + height) / 2;
            lbl.AutoSize = false;
            lbl.Location = new Point(centerX, centerY);
            lbl.TextAlign = ContentAlignment.MiddleCenter;
            lbl.BringToFront();
        }

        private void SelectValue(object sender, EventArgs e)
        {

        }

        private void ListBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
